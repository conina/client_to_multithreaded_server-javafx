package my_first_client_for_multithreaded_server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class MyClient {


		private Socket my_socket = null;
		private DataInputStream cin_stream = null;
		private DataOutputStream cout_stream = null;

		public MyClient (String address, int port) throws IOException {

			my_socket = new Socket(address, port);
			cin_stream = new DataInputStream(my_socket.getInputStream());
			cout_stream = new DataOutputStream(my_socket.getOutputStream());

			Scanner user_input_scanner = new Scanner(System.in);
			Scanner server_response_scanner = new Scanner(cin_stream);
			PrintWriter to_server_writer = new PrintWriter(new OutputStreamWriter(cout_stream),true);

			String user_input = "";
			String server_response ="";
			System.out.println(server_response_scanner.nextLine());

			while (!user_input.equalsIgnoreCase("over")) {
				user_input = user_input_scanner.nextLine();
				to_server_writer.println(user_input);
				server_response = server_response_scanner.nextLine();
				
				System.out.println(server_response);
				
			}
			
			System.out.println("Connection closed.");

			
			cin_stream.close();
			cout_stream.close();
			my_socket.close();

		}
	}

