package my_first_client_for_multithreaded_server;
import java.io.IOException;

public class ClientMainApp {

		public static void main (String[] arg) {
			
			try {
				int port = 2001;
				String address = "127.0.0.1";
				MyClient client = new MyClient(address, port);
				
			} catch (IOException e) {
				e.printStackTrace();
				
			}
			
		}
	}

